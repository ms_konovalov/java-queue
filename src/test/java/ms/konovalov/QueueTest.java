package ms.konovalov;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.*;

import static org.mockito.Mockito.*;

public class QueueTest {

    private final Clock mock = mock(Clock.class);
    private List<QueueService> services = new ArrayList<QueueService>() {{
        add(new InMemoryQueueService(mock));
        add(new FileQueueService(System.getProperty("java.io.tmpdir"), mock));
    }};

    @Test
    public void testSaveAndGet() throws QueueServiceException {
        for (QueueService service : services) {
            saveAndGet(service);
        }
    }

    private void saveAndGet(QueueService service) throws QueueServiceException {
        String queue = "queue" + File.separator + UUID.randomUUID();
        try {
            service.getOrCreateQueue(queue);
            Instant time = Instant.now();
            when(mock.millis()).thenReturn(time.toEpochMilli());
            MessageResult result = service.push(queue, new PushMessageRequest("1", Duration.ofMinutes(10), "blablabla"));
            Assert.assertEquals(1, result.getSuccessIds().size());
            result = service.push(queue, new PushMessageRequest("2", Duration.ofMinutes(30), "blablabla2"));
            Assert.assertEquals(1, result.getSuccessIds().size());
            result = service.push(queue, new PushMessageRequest("3", Duration.ofSeconds(30), "blablabla3"));
            Assert.assertEquals(1, result.getSuccessIds().size());
            result = service.push(queue, new PushMessageRequest("4", null, "blablabla4"));
            Assert.assertEquals(1, result.getSuccessIds().size());
            time = time.plus(Duration.ofMinutes(15));
            when(mock.millis()).thenReturn(time.toEpochMilli());
            MessageHandlerItem[] pull = service.pull(queue, 10, Duration.ofMinutes(1));
            Assert.assertEquals(3, pull.length);
        } finally {
            service.clean(queue);
        }
    }

    @Test
    public void testSaveDataAndDelete() throws QueueServiceException {
        for (QueueService service : services) {
            saveDataAndDelete(service);
        }
    }

    private void saveDataAndDelete(QueueService service) throws QueueServiceException {
        String queue = "queue" + File.separator + UUID.randomUUID();
        try {
            Instant time = Instant.now();
            when(mock.millis()).thenReturn(time.toEpochMilli());
            service.push(queue, new PushMessageRequest("1", null, "blablabla"));
            service.push(queue, new PushMessageRequest("2", Duration.ofSeconds(30), "blablabla2"));
            service.push(queue, new PushMessageRequest("3", Duration.ofSeconds(30), "blablabla3"));
            service.push(queue, new PushMessageRequest("4", null, "blablabla4"));
            service.push(queue, new PushMessageRequest("5", Duration.ofSeconds(3), "blablabla4"));
            time = time.plus(Duration.ofSeconds(10));
            when(mock.millis()).thenReturn(time.toEpochMilli());
            MessageHandlerItem[] pull = service.pull(queue, 10, Duration.ofMinutes(1));
            Assert.assertEquals(3, pull.length);
            time = time.plus(Duration.ofSeconds(40));
            when(mock.millis()).thenReturn(time.toEpochMilli());
            int deleted = service.delete(queue, pull).getSuccessIds().size();
            Assert.assertEquals(3, deleted);
            time = time.plus(Duration.ofMinutes(5));
            when(mock.millis()).thenReturn(time.toEpochMilli());
            pull = service.pull(queue, 10, Duration.ofMinutes(1));
            Assert.assertEquals(2, pull.length);
        } finally {
            service.clean(queue);
        }
    }

    @Test
    public void testSaveDataAndBlockFromDeletion() throws QueueServiceException {
        for (QueueService service : services) {
            saveDataAndBlockFromDeletion(service);
        }
    }

    private void saveDataAndBlockFromDeletion(QueueService service) throws QueueServiceException {
        String queue = "queue" + File.separator + UUID.randomUUID();
        try {
            Instant time = Instant.now();
            when(mock.millis()).thenReturn(time.toEpochMilli());
            MessageResult result = service.push(queue,
                    new PushMessageRequest("1", null, "blablabla"),
                    new PushMessageRequest("2", Duration.ofSeconds(30), "blablabla2"),
                    new PushMessageRequest("3", Duration.ofSeconds(30), "blablabla3"),
                    new PushMessageRequest("4", null, "blablabla4"));
            Assert.assertEquals(4, result.getSuccessIds().size());
            service.push(queue, new PushMessageRequest("5", Duration.ofSeconds(3), "blablabla4"));
            time = time.plus(Duration.ofSeconds(10));
            when(mock.millis()).thenReturn(time.toEpochMilli());
            MessageHandlerItem[] pull = service.pull(queue, 10, Duration.ofMinutes(1));
            Assert.assertEquals(3, pull.length);
            time = time.plus(Duration.ofSeconds(70));
            when(mock.millis()).thenReturn(time.toEpochMilli());
            service.pull(queue, 10, Duration.ofMinutes(0));
            int deleted = service.delete(queue, pull).getSuccessIds().size();
            Assert.assertEquals(0, deleted);
            time = time.plus(Duration.ofMinutes(5));
            when(mock.millis()).thenReturn(time.toEpochMilli());
            pull = service.pull(queue, 10, Duration.ofMinutes(1));
            Assert.assertEquals(5, pull.length);
        } finally {
            service.clean(queue);
        }
    }

    @Test
    public void testUnableToDeleteNotVisible() throws QueueServiceException {
        for (QueueService service : services) {
            unableToDeleteNotVisible(service);
        }
    }

    private void unableToDeleteNotVisible(QueueService service) throws QueueServiceException {
        String queue = "queue" + File.separator + UUID.randomUUID();
        try {
            Instant time = Instant.now();
            when(mock.millis()).thenReturn(time.toEpochMilli());
            MessageHandlerItem[] pull = service.pull(queue, 10, Duration.ofMinutes(1));
            Assert.assertEquals(0, pull.length);
            int deleted = service.delete(queue, pull).getSuccessIds().size();
            Assert.assertEquals(0, deleted);
            verify(mock, never()).millis();
        } finally {
            service.clean(queue);
        }
    }

    @Test
    public void testMultiThreaded() throws QueueServiceException, ExecutionException, InterruptedException {
        for (QueueService service : services) {
            multiThreaded(service);
        }
    }

    private void multiThreaded(QueueService service) throws InterruptedException, ExecutionException, QueueServiceException {
        String queue = "queue" + File.separator + UUID.randomUUID();
        try {
            Instant time = Instant.now();
            when(mock.millis()).thenReturn(time.toEpochMilli());
            ExecutorService executor = Executors.newFixedThreadPool(20);

            CompletableFuture<List<MessageResult>> pushResultFuture = Utils.sendParallel(service, queue, executor, 10);
            Utils.receiveParallel(service, queue, executor, 10);

            List<MessageResult> messageResults = pushResultFuture.get();

            time = time.plus(Duration.ofSeconds(70));
            when(mock.millis()).thenReturn(time.toEpochMilli());
            MessageHandlerItem[] pull = service.pull(queue, 10, Duration.ofMinutes(0));
            Assert.assertEquals(messageResults.size(), pull.length);
        } finally {
            service.clean(queue);
        }
    }


}
