package ms.konovalov;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class Utils {

    static CompletableFuture<Void> receiveParallel(QueueService service, String queue, ExecutorService executor, int count) {
        CompletableFuture[] array = IntStream.range(0, count).mapToObj(i -> CompletableFuture.runAsync(() -> {
            try {
                service.pull(queue, 1, null);
            } catch (QueueServiceException ignored) {
            }
        }, executor)).toArray(CompletableFuture[]::new);
        return CompletableFuture.allOf(array);
    }

    static CompletableFuture<List<MessageResult>> sendParallel(QueueService service, String queue, ExecutorService executor, int count) {

        CompletableFuture<MessageResult>[] array = IntStream.range(0, count).mapToObj(i -> CompletableFuture.supplyAsync(() -> {
            try {
                return service.push(queue, new PushMessageRequest("" + i, null, "qwerty" + i));
            } catch (QueueServiceException ignored) {
                ignored.printStackTrace();
                return new MessageResult[0];
            }
        }, executor)).toArray((IntFunction<CompletableFuture<MessageResult>[]>) CompletableFuture[]::new);

        return CompletableFuture.allOf(array).thenApply(v -> Arrays.stream(array).map(CompletableFuture::join).collect(Collectors.toList()));
    }
}
