package ms.konovalov;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQSClient;
import org.junit.Assert;
import org.junit.Test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SqsQueueServiceIT {

    private final String sqsUrl = System.getProperty("sqs.url") != null ? System.getProperty("sqs.url") : "http://localhost:9324";
    private final AmazonSQSClient sqsClient = new AmazonSQSClient(new BasicAWSCredentials("x", "x")).withEndpoint(sqsUrl);
    private final QueueService service = new SqsQueueService(sqsClient);

    @Test
    public void saveAndGetTest() throws QueueServiceException, InterruptedException {
        String queue = "queue" + UUID.randomUUID();
        try {
            service.push(queue, new PushMessageRequest("1", Duration.ofMinutes(10), "blablabla"));
            service.push(queue, new PushMessageRequest("2", Duration.ofMinutes(30), "blablabla2"));
            service.push(queue, new PushMessageRequest("3", Duration.ofSeconds(1), "blablabla3"));
            service.push(queue, new PushMessageRequest("4", null, "blablabla4"));
            Thread.sleep(1000);
            MessageHandlerItem[] pull = service.pull(queue, 10, Duration.ofMinutes(10));
            Assert.assertEquals(2, pull.length);
            int deleted = service.delete(queue, pull).getSuccessIds().size();
            Assert.assertEquals(2, deleted);
        } finally {
            service.clean(queue);
        }
    }

    @Test
    public void multiThreadedTest() throws InterruptedException, ExecutionException, QueueServiceException {
        Thread.sleep(5000);
        String queue = "queue" + UUID.randomUUID();
        service.getOrCreateQueue(queue);
        try {
            ExecutorService executor = Executors.newFixedThreadPool(20);
            CompletableFuture<List<MessageResult>> future = Utils.sendParallel(service, queue, executor, 7);
            Utils.receiveParallel(service, queue, executor, 18);

            List<MessageResult> messageResults = future.get();
            final int expectedSize = messageResults.size();

            List<MessageHandlerItem> result = new ArrayList<>();
            int attempt = 0;
            final int MAX_ATTEMPTS = 3;

            while (result.size() != expectedSize && attempt < MAX_ATTEMPTS) {
                Thread.sleep(1000);
                result.addAll(Arrays.asList(service.pull(queue, 10, Duration.ofMinutes(5))));
                attempt++;
            }

            Assert.assertEquals(expectedSize, result.size());

        } finally {
            service.clean(queue);
        }
    }

}
