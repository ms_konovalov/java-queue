package ms.konovalov;

import lombok.RequiredArgsConstructor;

import java.time.Clock;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

/**
 * {@inheritDoc}
 * <p>
 * In-memory implementation of {@link QueueService}
 * Uses separate {@link ReentrantLock} for each queue. Is blocking in aspect of obtaining this lock.
 */
@RequiredArgsConstructor
public class InMemoryQueueService implements QueueService {

    private final Clock clock;
    private final Map<String, Lock> locks = new ConcurrentHashMap<>();
    private final Map<String, List<DataItem>> storage = new ConcurrentHashMap<>();

    /**
     * {@inheritDoc}
     * <p>
     * Adds items into queue
     */
    @Override
    public MessageResult push(String queueUrl, PushMessageRequest... messages) {
        Lock lock = lock(queueUrl);
        try {
            List<DataItem> queueData = storage.computeIfAbsent(queueUrl, k -> new ArrayList<>());
            Arrays.stream(messages)
                    .map(this::createRecord)
                    .forEach(queueData::add);
            List<String> success = Arrays.stream(messages).map(PushMessageRequest::getId).collect(Collectors.toList());
            return new MessageResult(success, Collections.emptyList());
        } finally {
            lock.unlock();
        }
    }

    private DataItem createRecord(PushMessageRequest message) {
        long visibleFrom = calculateVisible(message.getDelay());
        return new DataItem(0, visibleFrom, null, message.getData());
    }

    private long calculateVisible(Duration delay) {
        return (delay != null) ? clock.millis() + delay.toMillis() : 0L;
    }

    /**
     * {@inheritDoc}
     * <p>
     * Clone items into new collection that should not be deleted, Count deleted items
     * Then put new collection instead of old one
     */
    @Override
    public MessageResult delete(String queueUrl, MessageHandlerItem... messageHandlers) {
        Lock lock = lock(queueUrl);
        try {
            List<String> success = new ArrayList<>();
            Map<String, String> handlers = Arrays.stream(messageHandlers).collect(Collectors.toMap(MessageHandlerItem::getHandler, MessageHandlerItem::getId));
            if (storage.containsKey(queueUrl)) {
                List<DataItem> queueData = storage.get(queueUrl);
                List<DataItem> tempData = new ArrayList<>();
                queueData.stream()
                        .filter(line -> {
                            if (handlers.containsKey(line.getHandler())) {
                                success.add(handlers.get(line.getHandler()));
                                handlers.remove(line.getHandler());
                                return false;
                            } else {
                                return true;
                            }
                        })
                        .forEach(tempData::add);
                storage.replace(queueUrl, tempData);
            }
            return new MessageResult(success, new ArrayList<>(handlers.values()));
        } finally {
            lock.unlock();
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * Clone invisible items into new collection, adds visible items with new 'visibility' param and generated 'handler' param
     * Then put new collection instead of old one
     * Returns collected messages with generated handlers
     * If queue doesn't exist returns empty array
     */
    @Override
    public MessageHandlerItem[] pull(String queueUrl, int maxMessages, Duration takeFor) {
        Lock lock = lock(queueUrl);
        try {
            List<MessageHandlerItem> taken = new ArrayList<>(maxMessages);
            List<DataItem> tempData = new ArrayList<>();
            if (storage.containsKey(queueUrl)) {
                List<DataItem> queueData = storage.get(queueUrl);
                queueData.stream()
                        .map(parsed -> {
                            if (isVisible(parsed) && taken.size() < maxMessages) {
                                DataItem newLine = new DataItem(parsed.getAttempt() + 1, calculateVisible(takeFor), generateHandler(), parsed.getData());
                                taken.add(new MessageHandlerItem(newLine.getHandler(), newLine.getData()));
                                return newLine;
                            }
                            return parsed;
                        })
                        .forEach(tempData::add);
                storage.replace(queueUrl, tempData);
            }
            return taken.toArray(new MessageHandlerItem[taken.size()]);
        } finally {
            lock.unlock();
        }
    }

    private String generateHandler() {
        return UUID.randomUUID().toString();
    }

    private boolean isVisible(DataItem line) {
        return line.getVisibleFrom() <= clock.millis();
    }

    /**
     * {@inheritDoc}
     * <p>
     * Removes all messages for this queue from storage
     * just for simplification lock will still exist in locks collection
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    public void clean(String queueUrl) {
        Lock lock = lock(queueUrl);
        try {
            storage.remove(queueUrl);
        } finally {
            lock.unlock();
        }
    }

    /*
     * Obtains lock for a queue
     * If lock doesn't exists new one will be created atomically
     */
    private Lock lock(String queueUrl) {
        Lock lock = locks.computeIfAbsent(queueUrl, key -> new ReentrantLock());
        lock.lock();
        return lock;
    }

    /**
     * {@inheritDoc}
     *
     * Doesn't create anything. Just returns name as URL
     * Not necessary to create anything as {@link InMemoryQueueService#lock} is atomic
     */
    public String getOrCreateQueue(String queueName) {
        return queueName;
    }
}
