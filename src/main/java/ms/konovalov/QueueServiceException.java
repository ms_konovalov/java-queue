package ms.konovalov;

public class QueueServiceException extends Exception {

    public QueueServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public QueueServiceException(String message) {
        super(message);
    }
}
