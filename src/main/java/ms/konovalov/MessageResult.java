package ms.konovalov;

import lombok.Data;

import java.util.List;

/**
 * Result of message operation request. Contains list of successfully added message identifiers and failed messages identifiers
 */
@Data
public class MessageResult {

    private final List<String> successIds;
    private final List<String> failedIds;
}
