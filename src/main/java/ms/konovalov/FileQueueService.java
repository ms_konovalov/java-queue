package ms.konovalov;

import lombok.AllArgsConstructor;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Clock;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

/**
 * {@inheritDoc}
 *
 * File system based {@link QueueService} implementation
 * Creates separate directory inside {@link FileQueueService#rootDir} for each queue
 * Uses .lock file as a lock for each queue separately. Is blocking in aspect of obtaining this lock
 * Can be used from different JVM instances.
 */
@AllArgsConstructor
public class FileQueueService implements QueueService {

    private final String rootDir;
    private final Clock clock;

    /**
     * {@inheritDoc}
     *
     * Appends lines to the file. Atomic (in case of error all changes will be lost)
     * If file doesn't exist creates it.
     * Creates file ".lock" in corresponding directory as a lock
     */
    @Override
    public MessageResult push(String queueUrl, PushMessageRequest... messages) throws QueueServiceException {
        String dir = getQueueDir(queueUrl);
        File queueFile = getQueueFile(dir);
        File lockFile = lock(dir);
        try (PrintWriter pw = new PrintWriter(new FileWriter(queueFile, true))) {
            Arrays.stream(messages)
                    .map(this::createRecord)
                    .forEach(parsed -> appendLine(pw, parsed.toString()));
            List<String> success = Arrays.stream(messages).map(PushMessageRequest::getId).collect(Collectors.toList());
            return new MessageResult(success, Collections.emptyList());
        } catch (IOException e) {
            throw new QueueServiceException("Unable to append to file " + queueFile.getPath(), e);
        } finally {
            unlock(lockFile);
        }
    }

    private void appendLine(PrintWriter writer, String record) {
        writer.println(record);
    }

    private DataItem createRecord(PushMessageRequest message) {
        long visibleFrom = calculateVisible(message.getDelay());
        return new DataItem(0, visibleFrom, null, message.getData());
    }

    /*
     * Calculate time from which item will be visible
     */
    private long calculateVisible(Duration delay) {
        return (delay != null) ? clock.millis() + delay.toMillis() : 0L;
    }

    /**
     * {@inheritDoc}
     *
     * Clones items that should not be deleted to separate temp file, counts amount of deleted files
     * Then swaps files atomically
     * In case of errors all changes will be lost
     * If queue file doesn't exist returns empty array
     */
    @Override
    public MessageResult delete(String queueUrl, MessageHandlerItem... messageHandlers) throws QueueServiceException {
        String dir = getQueueDir(queueUrl);
        File queueFile = getQueueFile(dir);
        File tempFile = createTempFile(dir);
        File lockFile = lock(dir);
        try (PrintWriter pw = new PrintWriter(new FileWriter(tempFile, true))) {
            List<String> success = new ArrayList<>();
            Map<String, String> handlers = Arrays.stream(messageHandlers).collect(Collectors.toMap(MessageHandlerItem::getHandler, MessageHandlerItem::getId));
            if (queueFile.exists()) {
                Files.lines(queueFile.toPath())
                        .map(DataItem::parse)
                        .filter(line -> {
                            String handler = line.getHandler();
                            if (handlers.containsKey(handler)) {
                                success.add(handlers.get(handler));
                                handlers.remove(handler);
                                return false;
                            } else {
                                return true;
                            }
                        })
                        .forEach(parsed -> appendLine(pw, parsed.toString()));
                substituteFile(queueFile, tempFile);
            }
            return new MessageResult(success, new ArrayList<>(handlers.values()));
        } catch (IOException e) {
            throw new QueueServiceException("Unable to read file " + queueFile, e);
        } finally {
            unlock(lockFile);
        }
    }

    /**
     * {@inheritDoc}
     *
     * Clones invisible items to temp file, adds handler identifier to visible lines and put them into same temp file then swaps files atomically
     * Visible items will be returned as array
     * In case of errors all changes will be lost
     * If queue file doesn't exist returns empty array
     */
    @Override
    public MessageHandlerItem[] pull(String queueUrl, int maxMessages, Duration takeFor) throws QueueServiceException {
        String dir = getQueueDir(queueUrl);
        File queueFile = getQueueFile(dir);
        File tempFile = createTempFile(dir);
        File lockFile = lock(dir);
        try (PrintWriter pw = new PrintWriter(new FileWriter(tempFile, true))) {
            List<MessageHandlerItem> taken = new ArrayList<>(maxMessages);
            if (queueFile.exists()) {
                // iterates over all lines in file
                Files.lines(queueFile.toPath())
                        .map(DataItem::parse)
                        //filters visible items untim maxMessages not reached
                        .map(parsed -> {
                            if (isVisible(parsed) && taken.size() < maxMessages) {
                                DataItem newLine = new DataItem(parsed.getAttempt() + 1, calculateVisible(takeFor), generateHandler(), parsed.getData());
                                taken.add(new MessageHandlerItem(newLine.getHandler(), newLine.getData()));
                                return newLine;
                            }
                            return parsed;
                        })
                        .forEach(parsed -> appendLine(pw, parsed.toString()));
                substituteFile(queueFile, tempFile);
            }
            return taken.toArray(new MessageHandlerItem[taken.size()]);
        } catch (IOException e) {
            throw new QueueServiceException("Unable to write to file " + tempFile.getPath(), e);
        } finally {
            unlock(lockFile);
        }
    }

    private String generateHandler() {
        return UUID.randomUUID().toString();
    }

    /*
     * Swaps source file to target file place atomically
     */
    private void substituteFile(File targetFile, File sourceFile) throws QueueServiceException {
        if (!sourceFile.renameTo(targetFile)) {
            throw new QueueServiceException("Unable to replace file " + targetFile.getPath());
        }
    }

    private boolean isVisible(DataItem line) {
        return line.getVisibleFrom() <= clock.millis();
    }

    /**
     * {@inheritDoc}
     *
     * Removes all files from directory, then removes .lock file and removes directory itself
     * Not safe to use in parallel with queue interaction from other clients
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    public void clean(String queueUrl) throws QueueServiceException {
        String dir = getQueueDir(queueUrl);
        File lockFile = lock(dir);
        try {
            Files.walk(Paths.get(dir), FileVisitOption.FOLLOW_LINKS)
                    .sorted(Comparator.reverseOrder())
                    .filter(file -> !file.endsWith(lockFile.getName()))
                    .map(Path::toFile)
                    .forEach(File::delete);
        } catch (IOException ignored) {
        } finally {
            unlock(lockFile);
            new File(dir).delete();
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private String getQueueDir(String queueUrl) {
        File dir = new File(rootDir + File.separator + queueUrl);
        dir.mkdirs();
        return dir.getPath();
    }

    private File createTempFile(String dir) throws QueueServiceException {
        return new File(dir + File.separator + UUID.randomUUID());
    }

    private File getQueueFile(String dir) throws QueueServiceException {
        return new File(dir + File.separator + "messages.txt");
    }

    private File getLockFile(String dir) throws QueueServiceException {
        return new File(dir + File.separator + ".lock");
    }

    /*
     * Try to create .lock file in a directory
     * Is blocking
     */
    private File lock(String dir) throws QueueServiceException {
        File lock = getLockFile(dir);
        try {
            while (!lock.createNewFile()) {
                Thread.sleep(50);
            }
            return lock;
        } catch (IOException | InterruptedException e) {
            throw new QueueServiceException("Unable to lock file " + lock.getPath(), e);
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void unlock(File lock) {
        lock.delete();
    }

    /**
     * {@inheritDoc}
     *
     * Is not necessary as {@link FileQueueService#getQueueDir(String)} is safe
     */
    public String getOrCreateQueue(String queueName) {
        getQueueDir(queueName);
        return queueName;
    }
}
