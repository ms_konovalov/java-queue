package ms.konovalov;

import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.*;
import lombok.RequiredArgsConstructor;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * {@inheritDoc}
 * <p>
 * AWS SQS implementation of {@link QueueService}
 */
@RequiredArgsConstructor
public class SqsQueueService implements QueueService {

    private final AmazonSQSClient sqsClient;

    /**
     * {@inheritDoc}
     * <p>
     * Not atomic, messages could be inserted partially
     */
    @Override
    public MessageResult push(String queueName, PushMessageRequest... messages) {
        String queueUrl = getOrCreateQueue(queueName);
        List<SendMessageBatchRequestEntry> collect = Arrays.stream(messages).map(message ->
                new SendMessageBatchRequestEntry()
                        .withId(message.getId())
                        .withMessageBody(message.getData())
                        .withDelaySeconds(message.getDelay() == null ? 0 : (int) message.getDelay().getSeconds())
        ).collect(Collectors.toList());
        SendMessageBatchResult sendMessageBatchResult = sqsClient.sendMessageBatch(new SendMessageBatchRequest().withQueueUrl(queueUrl).withEntries(collect));
        return new MessageResult(
                sendMessageBatchResult.getSuccessful().stream().map(SendMessageBatchResultEntry::getId).collect(Collectors.toList()),
                sendMessageBatchResult.getFailed().stream().map(BatchResultErrorEntry::getId).collect(Collectors.toList()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MessageHandlerItem[] pull(String queueName, int maxMessages, Duration takeFor) {
        String queueUrl = getOrCreateQueue(queueName);
        ReceiveMessageResult receiveMessageResult = sqsClient.receiveMessage(new ReceiveMessageRequest()
                .withQueueUrl(queueUrl)
                .withMaxNumberOfMessages(maxMessages)
                .withVisibilityTimeout(takeFor == null ? 0 : (int) takeFor.getSeconds())
        );
        return receiveMessageResult.getMessages().stream().map(message ->
                new MessageHandlerItem(message.getReceiptHandle(), message.getBody())).toArray(MessageHandlerItem[]::new);
    }

    /**
     * {@inheritDoc}
     * <p>
     * Not atomic, messages could be deleted partially
     */
    @Override
    public MessageResult delete(String queueName, MessageHandlerItem... messageHandlers) {
        String queueUrl = getOrCreateQueue(queueName);
        List<DeleteMessageBatchRequestEntry> collect = Arrays.stream(messageHandlers).map(handler ->
                new DeleteMessageBatchRequestEntry()
                        .withId(handler.getId())
                        .withReceiptHandle(handler.getHandler())
        ).collect(Collectors.toList());
        DeleteMessageBatchResult deleteMessageBatchResult = sqsClient.deleteMessageBatch(
                new DeleteMessageBatchRequest().withQueueUrl(queueUrl).withEntries(collect));
        return new MessageResult(
                deleteMessageBatchResult.getSuccessful().stream().map(DeleteMessageBatchResultEntry::getId).collect(Collectors.toList()),
                deleteMessageBatchResult.getFailed().stream().map(BatchResultErrorEntry::getId).collect(Collectors.toList()));
    }

    /**
     * {@inheritDoc}
     * <p>
     * Queue will be unavailable for re-creation for 60 sec
     */
    @Override
    public void clean(String queueName) {
        String queueUrl = getOrCreateQueue(queueName);
        sqsClient.deleteQueue(queueUrl);
    }

    /**
     * {@inheritDoc}
     */
    public String getOrCreateQueue(String queueName) {
        return sqsClient.createQueue(queueName).getQueueUrl();
    }
}
