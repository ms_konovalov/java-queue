package ms.konovalov;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Internal structure to store data.
 * Methods {@link DataItem#toString()} and {@link DataItem#parse(String)} are necessary for serialization into file
 */
@Data
@AllArgsConstructor
class DataItem {

    private int attempt;
    private long visibleFrom;
    private String handler;
    private String data;

    public String toString() {
        return attempt + ":" + visibleFrom + ":" + (handler == null ? "" : handler) + ":" + data;
    }

    static DataItem parse(String line) {
        if (line != null) {
            String[] array = line.split(":");
            return new DataItem(Integer.parseInt(array[0]), Long.parseLong(array[1]), array[2], array[3]);
        }
        return null;
    }
}
