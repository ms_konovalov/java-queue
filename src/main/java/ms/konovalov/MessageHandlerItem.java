package ms.konovalov;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

/**
 * Structure with data about message taken from queue
 */
@Data
public class MessageHandlerItem {

    /** Identifier of item in request to correlate with operation result */
    private final String id = UUID.randomUUID().toString();
    /** Identifier that used as a key for further deletion */
    private final String handler;
    private final String data;
}
