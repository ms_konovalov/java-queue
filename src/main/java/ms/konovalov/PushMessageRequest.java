package ms.konovalov;

import lombok.Data;

import java.time.Duration;

/**
 * Request for sending message to queue.
 */
@Data
public class PushMessageRequest {

    /** Message identifier. Should be used to correlate with response */
    private final String id;
    /** Duration to be used as visibility timeout starting from which message will be visible for pull */
    private final Duration delay;
    /** Message text */
    private final String data;
}
