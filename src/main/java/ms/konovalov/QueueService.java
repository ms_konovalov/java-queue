package ms.konovalov;

import java.time.Duration;

/**
 * Universal interface for interaction with Queue
 * Provides at least one delivery to the consumer
 * Supplier can set visibility timeout for each message independently
 * Deletion message from queue is a separate operation that can be done only providing message handler
 * identifier obtained while taking message from queue to guarantee safety
 */
public interface QueueService {

    /**
     * Push messages to queue
     *
     * @param queueUrl queue identifier
     * @param messages list of messages to push
     * @return result containing ids of failed and succeded messages
     * @throws QueueServiceException in case of inability to put messages in queue
     */
    MessageResult push(String queueUrl, PushMessageRequest... messages) throws QueueServiceException;

    /**
     * Take messages from queue
     *
     * @param queueUrl    queue identifier
     * @param maxMessages max amount of messqages to take
     * @param takeFor     duration taken messages will be invisible for other clients
     * @return array of messages taken from queue
     * @throws QueueServiceException in case of inability to take messages in queue
     */
    MessageHandlerItem[] pull(String queueUrl, int maxMessages, Duration takeFor) throws QueueServiceException;

    /**
     * Delete messages from queue
     *
     * @param queueUrl        queue identifier
     * @param messageHandlers array of message data taken from queue
     * @return result containing ids of failed and succeded messages
     * @throws QueueServiceException in case of inability to delete messages in queue
     */
    MessageResult delete(String queueUrl, MessageHandlerItem... messageHandlers) throws QueueServiceException;

    /**
     * Delete queue.
     * Unsafe! Should not be done during parallel interaction with queue
     *
     * @param queueUrl queue identifier
     * @throws QueueServiceException in case of inability to delete queue
     */
    void clean(String queueUrl) throws QueueServiceException;

    /**
     * Create queue.
     * Unsafe! Should not be done in parallel
     *
     * @param queueName queue identifier
     * @return queue URL to use in all other methods
     * @throws QueueServiceException in case of inability to create queue
     */
    String getOrCreateQueue(String queueName) throws QueueServiceException;

}
